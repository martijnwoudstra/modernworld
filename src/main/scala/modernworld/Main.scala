package modernworld

import cpw.mods.fml.common.event.{FMLFingerprintViolationEvent, FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}
import cpw.mods.fml.common.registry.GameRegistry
import cpw.mods.fml.common.{Mod, SidedProxy}
import modernworld.blocks.Asphalt
import modernworld.core.CommonProxy
import net.minecraft.block.Block
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.init.{Blocks, Items}
import net.minecraft.item.{ItemStack, Item}
import org.apache.logging.log4j.{Level, LogManager, Logger}

/**
 * Project: Modern World
 * Filename: Mod
 *
 * Created by: Martijn Woudstra
 * Date: 5-8-2014
 * Time: 17:38
 * Licensed under: GPL v3
 */
@Mod(modid = Constants.Modid, name = Constants.Modname, version = Constants.Version, modLanguage = "scala")
object Main {
  @SidedProxy(clientSide = Constants.ClientProxy, serverSide = Constants.CommonProxy)
  var proxy: CommonProxy = null

  var logger: Logger = LogManager.getLogger(Constants.Modid)

  @Mod.EventHandler
  def invalidFingerprint(event: FMLFingerprintViolationEvent) {
    if (Constants.Fingerprint == "@FINGERPRINT@")
      logger.log(Level.ERROR, "WARNING, THIS IS NOT A VALID RELEASE! Download from a real mirror!")
  }

  @Mod.EventHandler
  def preInit(event: FMLPreInitializationEvent) {
    ModBlocks.addBlocks()

    ModItems.addItems()

    CreativeTabModernWorld
  }

  @Mod.EventHandler
  def init(event: FMLInitializationEvent) {
    RecipeHandler.addRecipes()
  }

  @Mod.EventHandler
  def postInit(event: FMLPostInitializationEvent) {

  }
}

object RecipeHandler {
  def addRecipes(){
    GameRegistry.addRecipe(new ItemStack(Asphalt, 1, 0), "X  ","Y  ", 'x', Tar, 'Y', Blocks.stone)
    GameRegistry.addRecipe(new ItemStack(Asphalt, 1, 0), " X "," Y ", 'x', Tar, 'Y', Blocks.stone)
    GameRegistry.addRecipe(new ItemStack(Asphalt, 1, 0), "  X","  Y", 'x', Tar, 'Y', Blocks.stone)
  }
}
object Constants {
  final val Fingerprint = "@FINGERPRINT@"
  final val Modid = "modernworld"
  final val Modname = "Modern World"
  final val Version = "@VERSION@"
  final val ClientProxy = "modernworld.core.ClientProxy"
  final val CommonProxy = "modernworld.core.CommonProxy"
}

object ModBlocks {
  def addBlocks() {
    Asphalt
  }
}

object ModItems {
  def addItems() {

  }
}

object CreativeTabModernWorld extends CreativeTabs(Constants.Modid) {
  override def getTabIconItem: Item = Items.apple //TODO
}