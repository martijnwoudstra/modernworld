package modernworld.blocks

import net.minecraft.block.Block

/**
 * Project: Modern World
 * Filename: ModernBlock
 *
 * Created by: Martijn Woudstra
 * Date: 5-8-2014
 * Time: 18:32
 * Licensed under: GPL v3
 */
trait ModernBlock extends Block {
  def getName: String
}