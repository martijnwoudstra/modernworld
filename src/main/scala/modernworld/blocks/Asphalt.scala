package modernworld.blocks

import cpw.mods.fml.common.registry.GameRegistry
import modernworld.{Constants, CreativeTabModernWorld}
import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.client.renderer.texture.IIconRegister
import net.minecraft.util.IIcon
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection

/**
 * Project: Modern World
 * Filename: Asphalt
 *
 * Created by: Martijn Woudstra
 * Date: 5-8-2014
 * Time: 18:31
 * Licensed under: GPL v3
 */

trait AsphaltBlock extends Block with ModernBlock{ def getIsPainted: Boolean}

object Asphalt extends Block(Material.rock) with AsphaltBlock{
  /*
  0: Empty
  1: Line NS
  2: Line EW
  3: Curve NE
  4: Curve NW
  5: Curve SE
  6: Curve SW
  7: T !N
  8: T !E
  9: T !S
  10: T !W
   */
  setBlockName(getName)
  setCreativeTab(CreativeTabModernWorld)
  GameRegistry.registerBlock(this, getName)

  var isPainted = false
  val iconCount = 5
  var icons: Array[IIcon] = new Array[IIcon](iconCount)

  def getIsPainted: Boolean = isPainted

  override def getName: String = "asphalt"

  override def registerBlockIcons(iconRegister: IIconRegister): Unit = {
    for (i <- 0 until iconCount)
      iconRegister.registerIcon(Constants.Modid + ":" + getName + getName(i))
  }

  override def getIcon(side: Int, meta: Int): IIcon = {
    if(side != 1)
      icons(0)
    else icons(meta + 1)
  }

  override def onBlockPlaced(world : World, x : Int, y : Int, z : Int, side : Int, xHit : Float, yHit : Float, zHit : Float, meta : Int): Int = {
    var info: Byte = 0
    if (world.getBlock(x, y, z - 1) == Asphalt && world.getBlock(x, y, z - 1).asInstanceOf[AsphaltBlock].getIsPainted) {
      info.+(1)
    }
    if (world.getBlock(x, y, z + 1) == Asphalt && world.getBlock(x, y, z + 1).asInstanceOf[AsphaltBlock].getIsPainted) {
      info.+(2)
    }
    if (world.getBlock(x + 1, y, z) == Asphalt && world.getBlock(x + 1, y, z).asInstanceOf[AsphaltBlock].getIsPainted) {
      info.+(4)
    }
    if (world.getBlock(x - 1, y, z) == Asphalt && world.getBlock(x - 1, y, z).asInstanceOf[AsphaltBlock].getIsPainted) {
      info.+(8)
    }
    info match {
      case 1 | 2| 3 => 1 //Line
      case 12 | 4 | 8 => 2 //Curve
      case _ => info.toInt + 2 //T
    }
  }

  override def onNeighborBlockChange(world : World, x : Int, y : Int, z : Int, block : Block): Unit = {
    onBlockPlaced(world, x, y, z, 0, 0, 0, 0, world.getBlockMetadata(x, y, z))
  }
}
